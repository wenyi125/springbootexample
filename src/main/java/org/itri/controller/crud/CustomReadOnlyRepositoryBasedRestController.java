/*
 * Copyright© Institute for Information Industry
 * All rights reserved.
 * 本程式碼係屬財團法人資訊工業策進會版權所有，在未取得本會書面同意前，不得複製、散佈或修改。
 */
package org.itri.controller.crud;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.itri.controller.crud.RestController;
import org.itri.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

/**
 * 程式資訊摘要：<P>
 * 類別名稱　　：CustomReadOnlyRepositoryBasedRestController.java<P>
 * 程式內容說明：<P>
 * 程式修改記錄：<P>
 * date：2016年3月4日<P>
 *@author arderliu
 *@version 1.0
 *@since 1.0
 */
public abstract class CustomReadOnlyRepositoryBasedRestController<T, ID extends Serializable, R extends PagingAndSortingRepository> implements ReadOnlyRestController<T, ID> {

    protected R repository;
    protected T t;

    protected Logger logger = LoggerFactory.getLogger(CustomReadOnlyRepositoryBasedRestController.class);

    /**
     * You should override this setter in order to inject your repository with @Inject annotation
     *
     * @param repository The repository to be injected
     */
    public void setRepository(R repository) {
        this.repository = repository;
    }




    /**
     * {@inheritDoc}
     */
    @Override
    public Iterable<T> findAll() {
        return repository.findAll();
    }

    /**
     * {@inheritDoc}
     */
//    @Override
//    @RequestMapping(method=RequestMethod.GET,value="/paging")
//    @ApiImplicitParams({
//        @ApiImplicitParam(name = "direction", value = "direction", required = false, defaultValue = "ASC",dataType = "string", paramType = "query"),
//        @ApiImplicitParam(name = "properties", value = "column name", required = false, defaultValue = "id",dataType = "string", paramType = "query")
//    })
//    public Page<T> findPaginated(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
//                                 @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
//                                 @RequestParam(value = "direction", required = false, defaultValue = "") String direction,
//                                 @RequestParam(value = "properties", required = false) String properties) {
//        Assert.isTrue(page > 0, "Page index must be greater than 0");
//        Assert.isTrue(direction.isEmpty() || direction.equalsIgnoreCase(Sort.Direction.ASC.toString()) || direction.equalsIgnoreCase(Sort.Direction.DESC.toString()), "Direction should be ASC or DESC");
//        if(direction.isEmpty()) {
//            return this.repository.findAll(new PageRequest(page - 1, size));
//        } else {
//            Assert.notNull(properties);
//            return this.repository.findAll(new PageRequest(page - 1, size, new Sort(Sort.Direction.fromString(direction.toUpperCase()), properties.split(","))));
//        }
//    }
    
    @SuppressWarnings("unchecked")
    @Override
    @RequestMapping(method=RequestMethod.GET,value="/paging")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "direction", value = "direction", required = false, defaultValue = "ASC",dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "properties", value = "column name", required = false, defaultValue = "id",dataType = "string", paramType = "query")
    })
    public Page<T> findPaginated(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                 @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                                 @RequestParam(value = "direction", required = false, defaultValue = "") Sort.Direction direction,
                                 @RequestParam(value = "properties", required = false) String properties) {
        Assert.isTrue(page > 0, "Page index must be greater than 0");
        Assert.isTrue((direction == null || direction == Sort.Direction.ASC || direction == Sort.Direction.DESC), "Direction should be ASC or DESC");
        if(direction == null) {
            return this.repository.findAll(new PageRequest(page - 1, size));
        } else {
            Assert.notNull(properties);
            return this.repository.findAll(new PageRequest(page - 1, size, new Sort(direction, properties.split(","))));
        }
    }
    

    /**
     * 
     * @param columnName : 欄位名稱
     * @return T
     */

    @RequestMapping(method=RequestMethod.GET,value="/findLast")
    public @ResponseBody T findLastRecord(@RequestParam(value = "columnName", required = true, defaultValue = "id") String columnName) {

        final PageRequest pageable = new PageRequest(
                0, 1, new Sort(
                    new Order(Direction.DESC, columnName)
                  )
                );
        List<T> list = this.repository.findAll(pageable).getContent();
        if(list.size() > 0){
            return (T) this.repository.findAll(pageable).getContent().get(0);
        } else {
            return null;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public T findById(@PathVariable ID id) {
        T entity = (T)this.repository.findOne(id);
        if (entity == null) {
            throw new NotFoundException();
        }

        return entity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterable<T> findByIds(@RequestParam(value="ids[]") Set<ID> ids){
        Assert.notNull(ids, "ids list cannot be null");
        return this.repository.findAll(ids);
    }



}
