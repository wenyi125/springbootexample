/*
 * Copyright© Industrial Technology Research Institute, ITRI(工業技術研究院, 工研院)
 * All rights reserved.
 * 本程式碼係屬ITRI版權所有，在未取得本院單位書面同意前，不得複製、散佈或修改。
 */
package org.itri.exception;

/**
 * 程式資訊摘要：<P>
 * 類別名稱　　：NotFoundException.java<P>
 * 程式內容說明：<P>
 * 程式修改記錄：<P>
 * date：2016年8月27日<P>
 *@author A50319
 *@version 1.0
 *@since 1.0
 */
public class NotFoundException extends RuntimeException {
    
    public NotFoundException() {
        super();
    }

    public NotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(final String message) {
        super(message);
    }

    public NotFoundException(final Throwable cause) {
        super(cause);
    }
    
}