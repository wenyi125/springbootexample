/*
 * Copyright© Industrial Technology Research Institute, ITRI(工業技術研究院, 工研院)
 * All rights reserved.
 * 本程式碼係屬ITRI版權所有，在未取得本院單位書面同意前，不得複製、散佈或修改。
 */
package com.practice.config;



import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 程式資訊摘要：<P>
 * 類別名稱　　：Swagger2SpringBoot.java<P>
 * 程式內容說明：<P>
 * 程式修改記錄：<P>
 * date：2016年8月26日<P>
 *@author A50319
 *@version 1.0
 *@since 1.0
 */

@SpringBootApplication
@EnableSwagger2
@ComponentScan("com.practice.controllor")
public class Swagger2SpringBoot {
    
      @Bean
      public Docket newsApi() {
          return new Docket(DocumentationType.SWAGGER_2)
                  .apiInfo(apiInfo())
                  .select()
                  .apis(RequestHandlerSelectors.any())              
                  .paths(PathSelectors.any())
                  .build();
      }
       
      private ApiInfo apiInfo() {
          return new ApiInfoBuilder()
                  .title("Spring REST Sample with Swagger")
                  .description("Spring REST Sample with Swagger")
                  .version("2.0")
                  .build();
      }
}