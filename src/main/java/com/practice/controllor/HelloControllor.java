package com.practice.controllor;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloControllor {
	@RequestMapping("/")
	public String home() {
		return "Hello World!";
	}
}
