package com.practice.controllor;

import org.itri.controller.crud.CustomReadOnlyRepositoryBasedRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practice.model.Role;
import com.practice.model.repo.RoleRepository;

@RestController
@RequestMapping(value = "/api/role")
public class RoleController extends CustomReadOnlyRepositoryBasedRestController<Role,Long,RoleRepository>{
	
	@Autowired
    @Override
    public void setRepository(RoleRepository repository){
        this.repository = repository;
    }


}

//
//
//@Autowired
//private JdbcTemplate jdbcTemplate;
//
//@RequestMapping(value = "/role")
//@ResponseBody
//public Role dailyStats(@RequestParam Integer id) {
//	String query = "SELECT name,id from role where role.id = " + id;
//
//	return jdbcTemplate.queryForObject(query, (resultSet, i) -> {
//		return new Role(resultSet.getString(1), resultSet.getInt(2));
//	});
//
//}
//
//@RequestMapping(value = "/insertRole")
//@ResponseBody
//public void createRole(@RequestParam String name, @RequestParam Integer id) {
//	jdbcTemplate.update("insert into role(name, id) values(?, ?)", name, id);
//}
